package ru.lemar98.walkthroughpoints.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import ru.lemar98.walkthroughpoints.Main;
import ru.lemar98.walkthroughpoints.utils.Currier;

public class Quite implements Listener {

    private Plugin plugin;

    public Quite(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void quite(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if(Main.getCache().containsKey(player.getUniqueId())) {
            Currier currier = new Currier(this.plugin, player);
            currier.stop();
        }
    }
}
