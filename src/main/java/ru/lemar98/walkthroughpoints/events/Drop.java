package ru.lemar98.walkthroughpoints.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import ru.lemar98.walkthroughpoints.Main;
import ru.lemar98.walkthroughpoints.utils.Currier;

public class Drop implements Listener {

    private Plugin plugin;

    public Drop(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void drop(PlayerDropItemEvent event) {
        Player player = event.getPlayer();
        ItemStack itemStack = event.getItemDrop().getItemStack();
        Currier currier = new Currier(this.plugin, player);
        if(Main.getCache().containsKey(player.getUniqueId())) {
            if(itemStack.equals(currier.getTargetCompass())) {
                event.setCancelled(true);
            }
        }
    }
}
