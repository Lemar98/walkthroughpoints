package ru.lemar98.walkthroughpoints.events;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import ru.lemar98.walkthroughpoints.Main;
import ru.lemar98.walkthroughpoints.utils.Currier;

public class InventoryClick implements Listener {

    private Plugin plugin;

    public InventoryClick(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        Currier currier = new Currier(this.plugin, player);
        ItemStack clicked = event.getCurrentItem();
        if (Main.getCache().containsKey(player.getUniqueId())) {
            if (clicked != null && clicked.equals(currier.getTargetCompass())) {
                event.setCancelled(true);
                if(player.getGameMode().equals(GameMode.CREATIVE)) {
                    player.closeInventory();
                }
            }
        }
    }
}
