package ru.lemar98.walkthroughpoints;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.walkthroughpoints.commands.WpMainCommand;
import ru.lemar98.walkthroughpoints.events.Drop;
import ru.lemar98.walkthroughpoints.events.InventoryClick;
import ru.lemar98.walkthroughpoints.events.Quite;
import ru.lemar98.walkthroughpoints.utils.Util;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public final class Main extends JavaPlugin {

    private static Economy econ = null;
    private static Map<UUID, Util> cache;

    @Override
    public void onEnable() {
        setupEconomy();
        saveDefaultConfig();
        cache = new HashMap<>();
        getCommand("wp").setExecutor(new WpMainCommand(this));
        Bukkit.getPluginManager().registerEvents(new Quite(this), this);
        Bukkit.getPluginManager().registerEvents(new InventoryClick(this), this);
        Bukkit.getPluginManager().registerEvents(new Drop(this), this);
    }

    public static Map<UUID, Util> getCache() {
        return cache;
    }

    public static Economy getEcon() {
        return econ;
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
}
