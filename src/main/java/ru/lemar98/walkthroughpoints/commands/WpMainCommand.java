package ru.lemar98.walkthroughpoints.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import ru.lemar98.walkthroughpoints.Main;
import ru.lemar98.walkthroughpoints.utils.Currier;
import ru.lemar98.walkthroughpoints.utils.MethodUtils;

public class WpMainCommand implements CommandExecutor {

    private Plugin plugin;
    private MethodUtils methodUtils;

    public WpMainCommand(Plugin plugin) {
        this.plugin = plugin;
        this.methodUtils = new MethodUtils(this.plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "У Вас недостаточно прав!");
            return true;
        }
        if(args.length > 2) {
            sender.sendMessage(ChatColor.RED + "Неверные аргументы");
            return true;
        }
        if(args.length == 0) {
            sender.sendMessage(ChatColor.GREEN + "========== " + ChatColor.BLUE + "POLITMINE" + ChatColor.GREEN + " ==========");
            sender.sendMessage(ChatColor.GOLD + "/wp start - начать работу");
            if(sender.isOp()) {
                sender.sendMessage(ChatColor.RED + "/wp setpoint <цена> - установить точку и цену");
            }
            sender.sendMessage(ChatColor.GREEN + "=========================");
            return true;
        }
        Player player = (Player)sender;
        if(args.length == 1) {
            if(args[0].equals("start")) {
                if(Main.getCache().containsKey(player.getUniqueId())) {
                    player.sendMessage(ChatColor.RED + "Вы уже работаете!");
                    return true;
                }
                Currier currier = new Currier(this.plugin, player);
                currier.start();
                return true;
            } else if(args[0].equals("stop")) {
                if(!Main.getCache().containsKey(player.getUniqueId())) {
                    player.sendMessage(ChatColor.RED + "Вы не начинали работу!");
                    return true;
                }
                Currier currier = new Currier(this.plugin, player);
                currier.stop();
                return true;
            } else {
                sender.sendMessage(ChatColor.RED + "Такого аргумента нет");
                return true;
            }
        } else if(args.length == 2) {
            if(!player.isOp()) {
                player.sendMessage(ChatColor.RED + "У Вас недостаточно прав!");
                return true;
            }
            if(args[0].equals("setpoint")) {
                if(this.isInt(args[1])) {
                    int price = Integer.parseInt(args[1]);
                    this.methodUtils.addLocation(player.getLocation(), price);
                    player.sendMessage(ChatColor.GREEN + "Точка была успешно установлена");
                    return true;
                } else {
                    sender.sendMessage(ChatColor.RED + "Такого аргумента нет");
                    return true;
                }
            }
        } else {
            sender.sendMessage(ChatColor.RED + "Такого аргумента нет");
            return true;
        }
        return true;
    }

    private boolean isInt(String msg) {
        try {
            Integer.parseInt(msg);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
}
