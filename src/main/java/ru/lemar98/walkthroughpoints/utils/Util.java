package ru.lemar98.walkthroughpoints.utils;

import org.bukkit.Location;

public class Util {

    private Location location;
    private int zp;

    public Util(Location location, int zp) {
        this.location = location;
        this.zp = zp;
    }

    public Location getLocation() {
        return location;
    }

    public int getZp() {
        return zp;
    }
}
