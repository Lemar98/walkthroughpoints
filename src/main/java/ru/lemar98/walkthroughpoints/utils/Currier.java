package ru.lemar98.walkthroughpoints.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import ru.lemar98.walkthroughpoints.Main;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Currier {

    private Plugin plugin;
    private Player player;

    public Currier(Plugin plugin, Player player) {
        this.plugin = plugin;
        this.player = player;
    }

    public void givePlayerNextLocation() {
        Util util = getUtil();
        if(util == null) {
            while (util == null) {
                util = getUtil();
            }
        }
        player.setCompassTarget(util.getLocation());
        Main.getCache().put(this.player.getUniqueId(), util);
        LocationTask locationTask = new LocationTask(this.plugin, util);
        locationTask.runTaskTimer(this.plugin, 40L, 40L);
    }

    public void start() {
        Inventory inventory = this.player.getInventory();
        if(!inventory.contains(getTargetCompass())) {
            if(inventory.firstEmpty() != -1) {
                inventory.addItem(getTargetCompass());
            } else {
                this.player.sendMessage(ChatColor.RED + "Освободите место в инвентаре!");
                return;
            }
        }
        Util util = getUtil();
        if(util == null) {
            while (util == null) {
                util = getUtil();
            }
        }
        this.player.setCompassTarget(util.getLocation());
        this.player.sendMessage(ChatColor.GREEN + "Вы начали работу! Вам нужно просто ходить по точкам. Удачи =)");
        Main.getCache().put(this.player.getUniqueId(), util);
        LocationTask locationTask = new LocationTask(this.plugin, util);
        locationTask.runTaskTimer(this.plugin, 40L, 40L);
    }

    public void stop() {
        if(Main.getCache().containsKey(this.player.getUniqueId())) {
            Inventory inventory = this.player.getInventory();
            if (inventory.contains(getTargetCompass())) {
                inventory.removeItem(getTargetCompass());
            }
            this.player.sendMessage(ChatColor.RED + "Вы закончили работу!");
            Main.getCache().remove(this.player.getUniqueId());
        }
    }

    private Util getUtil() {
        List<String> list = this.plugin.getConfig().getStringList("locations");
        Random random = new Random();
        int rand = random.nextInt(list.size());
        String[] locArgs = list.get(rand).split(";");
        Location location = new Location(Bukkit.getWorld(locArgs[0]), Double.parseDouble(locArgs[1]), Double.parseDouble(locArgs[2]), Double.parseDouble(locArgs[3]));
        if(location.distance(this.player.getLocation()) <= 3) return null;
        return new Util(location, Integer.parseInt(locArgs[4]));
    }

    public ItemStack getTargetCompass() {
        ItemStack itemStack = new ItemStack(Material.COMPASS, 1);
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Указатель на следующую точку");
        meta.setLore(Arrays.asList(ChatColor.BLUE + "POLITMINE"));
        itemStack.setItemMeta(meta);
        return itemStack;
    }
}
