package ru.lemar98.walkthroughpoints.utils;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import ru.lemar98.walkthroughpoints.Main;

import java.util.List;

public class MethodUtils {

    private Plugin plugin;

    public MethodUtils(Plugin plugin) {
        this.plugin = plugin;
    }

    public void addLocation(Location location, int price) {
        List<String> cfgList = this.plugin.getConfig().getStringList("locations");
        String infoStr = location.getWorld().getName()+";"+location.getX()+";"+location.getY()+";"+location.getZ()+";"+price;
        cfgList.add(infoStr);
        this.plugin.getConfig().set("locations", cfgList);
        this.plugin.saveConfig();
    }

    public static void giveMoney(Player player, int amount) {
        Main.getEcon().depositPlayer(player, amount);
    }
}
