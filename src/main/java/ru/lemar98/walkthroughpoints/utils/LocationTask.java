package ru.lemar98.walkthroughpoints.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import ru.lemar98.walkthroughpoints.Main;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class LocationTask extends BukkitRunnable {

    private Plugin plugin;
    private Util util;
    private Player player;

    public LocationTask(Plugin plugin, Util util) {
        this.plugin = plugin;
        this.util = util;
        this.player = Bukkit.getPlayer(getKeyByValue(util));
    }

    @Override
    public void run() {
        if(Main.getCache().containsKey(player.getUniqueId())) {
            int distance = (int) player.getLocation().distance(util.getLocation());
            if (distance <= 3) {
                Main.getCache().remove(player.getUniqueId());
                MethodUtils.giveMoney(player, util.getZp());
                player.sendTitle(ChatColor.GREEN + "Вы прибыли на место назначения!", ChatColor.GOLD + "и получили " + ChatColor.RED + "" + util.getZp() + " валюты", 20,40,20);
                Currier currier = new Currier(this.plugin, player);
                currier.givePlayerNextLocation();
                cancel();
            } else {
                player.sendTitle(ChatColor.GREEN + "До нужной точки осталось", ChatColor.GOLD + "" + distance + ChatColor.GREEN + " блоков", 20, 40, 20);
            }
        } else {
            cancel();
        }
    }

    private UUID getKeyByValue(Util value) {
        Optional<UUID> result = Main.getCache().entrySet()
                .stream()
                .filter(entry -> value.equals(entry.getValue()))
                .map(Map.Entry::getKey)
                .findFirst();
        return result.get();
    }
}
